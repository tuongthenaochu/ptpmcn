<?php 
class CoursesController extends AppController{

	public $components = array('RequestHandler');

	public function index(){
		$courses = $this->Course->find('all');
		$this->set(array(
			'courses' => $courses,
			'_serialize' => array('courses')
		));
	}

	public function view($id) {
        $courses = $this->Course->findById($id);
        $this->set(array(
            'courses' => $courses,
            '_serialize' => array('courses')
        ));
    }
}
?>